//
//  xc_essentialsApp.swift
//  xc-essentials
//
//  Created by Tom Rogers on 5/24/22.
//

import SwiftUI

@main
struct xc_essentialsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
