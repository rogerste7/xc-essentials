//
//  ContentView.swift
//  xc-essentials
//
//  Created by Tom Rogers on 5/24/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world update!")
            .padding(.trailing)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
